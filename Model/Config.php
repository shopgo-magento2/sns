<?php
/**
 * Copyright © 2015 ShopGo. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ShopGo\AmazonSns\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Config model
 */
class Config extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var Config\System
     */
    public $systemConfig;

    /**
     * @var Config\File
     */
    public $fileConfig;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @param Config\System $systemConfig
     * @param \ShopGo\AmazonSns\Model\Config\File $fileConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    public function __construct(
        Config\System $systemConfig,
        \ShopGo\AmazonSns\Model\Config\File $fileConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        $this->systemConfig   = $systemConfig;
        $this->fileConfig     = $fileConfig;
        $this->_cacheTypeList = $cacheTypeList;
    }

    /**
     * Get config value
     *
     * @param string $path
     * @param string $scope
     * @param null|string $scopeCode
     * @return mixed
     */
    public function getConfigValue(
        $path = null,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        $config = $this->fileConfig->getConfigElementValue($path);
        return !$config
            ? $this->systemConfig->getConfigValue($path, $scope, $scopeCode)
            : $config;
    }

    /**
     * Set config value
     *
     * @param string $path
     * @param mixed $value
     * @param string $scope
     * @param null|string $scopeCode
     * @return void
     */
    public function setConfigValue(
        $path,
        $value,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        $this->systemConfig->setConfigValue($path, $value, $scope, $scopeCode);
        $this->systemConfig->clearConfigCache();
    }
}
