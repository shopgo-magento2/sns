<?php
/**
 * Copyright © 2015 ShopGo. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ShopGo\AmazonSns\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * System config model
 */
class System extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Magento\Framework\App\MutableScopeConfig
     */
    protected $_mutableScopeConfig;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @param \Magento\Framework\App\MutableScopeConfig $mutableScopeConfig
     */
    public function __construct(
        \Magento\Framework\App\MutableScopeConfig $mutableScopeConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        $this->_mutableScopeConfig = $mutableScopeConfig;
        $this->_cacheTypeList = $cacheTypeList;
    }

    /**
     * Get config value
     *
     * @param string $path
     * @param string $scope
     * @param null|string $scopeCode
     * @return mixed
     */
    public function getConfigValue(
        $path,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->_mutableScopeConfig->getValue($path, $scope, $scopeCode);
    }

    /**
     * Set config value
     *
     * @param string $path
     * @param mixed $value
     * @param string $scope
     * @param null|string $scopeCode
     * @return void
     */
    public function setConfigValue(
        $path,
        $value,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        $this->_mutableScopeConfig->setValue($path, $value, $scope, $scopeCode);
    }

    /**
     * Clear config cache
     *
     * @return void
     */
    public function clearConfigCache()
    {
        $this->_cacheTypeList->cleanType('config');
    }
}
